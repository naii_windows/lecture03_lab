﻿using Lecture3_MVVM_LabAdvanced.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_MVVM_LabAdvanced.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {

        public RelayCommand AllMoviesCommand { get; set; }

        private ViewModelBase _currentData;

        public ViewModelBase CurrentData
        {
            get { return _currentData; }
            set { _currentData = value; RaisePropertyChanged(); }
        }


        public MainPageViewModel()
        {
            AllMoviesCommand = new RelayCommand( _ => ShowMovies());
        }

        private void ShowMovies()
        {
            CurrentData = new MoviesViewModel();
            Debug.Write("Show movies called");
        }
    }
}
