﻿using Lecture3_MVVM_LabAdvanced.Model;
using Lecture3_MVVM_LabAdvanced.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture3_MVVM_LabAdvanced.ViewModel
{
    public class MoviesViewModel : ViewModelBase
    {
        public ObservableCollection<Movie> Movies { get; set; }
        public RelayCommand AddMovieCommand { get; set; }

        public MoviesViewModel()
        {
            Movies = new ObservableCollection<Movie>(DummyDataSource.Movies);
            AddMovieCommand = new RelayCommand((p) => AddMovie(p));
        }

        private void AddMovie(object p)
        {
            Movies.Add(new Movie() {
                Title = p.ToString(),
                Description = "Dummy description",
                Director = "Dummy Director"
            });

                        
        }


    }
}
