﻿using Lecture03_Lab.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Lecture03_Lab
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Page3 : Page
    {
        ObservableCollection<Player> items;
        public Page3()
        {
            this.InitializeComponent();

            items = new ObservableCollection<Player>()
            {
                new Player() {Name="Rik", Score=40 },
                new Player() {Name="Nena", Score=80 },
                new Player() {Name="John", Score=55 }
            };

            this.DataContext = items;

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            items.Add(new Player() { Name = "NewPlayer", Score = 100 });
        }
    }
}
