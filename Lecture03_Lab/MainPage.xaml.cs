﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Lecture03_Lab
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void HamburgerButton_Click(Object sender, RoutedEventArgs e)
        {
            mainSplitView.IsPaneOpen = !mainSplitView.IsPaneOpen;
        }

        private void StackPanel_Tapped(Object sender, TappedRoutedEventArgs e)
        {
            mainFrame.Navigate(typeof(Page1));
        }

        private void StackPanel_Tapped_1(Object sender, TappedRoutedEventArgs e)
        {
            mainFrame.Navigate(typeof(Page2));
        }

        private void StackPanel_Tapped_2(Object sender, TappedRoutedEventArgs e)
        {
            mainFrame.Navigate(typeof(Page3));
        }



    }
}
