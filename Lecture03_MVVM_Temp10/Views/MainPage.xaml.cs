﻿using System;
using Windows.UI.Xaml.Controls;

namespace Lecture03_MVVM_Temp10.Views
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
        }
    }
}