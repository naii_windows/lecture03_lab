﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03_Lab_MVVMLight.Models
{
    public class Movie : INotifyPropertyChanged
    {
        private String _title;

        public String Title
        {
            get { return _title; }
            set { _title = value; RaisePropertyChanged("Title"); }
        }

        private String _director;

        public String Director
        {
            get { return _director; }
            set { _director = value; RaisePropertyChanged("Director"); }
        }

        private String _description;

        public String Description
        {
            get { return _description; }
            set { _description = value; RaisePropertyChanged("Description"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
