﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Lecture03_Lab_MVVMLight.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03_Lab_MVVMLight.ViewModels
{
    public class MoviesViewModel : ViewModelBase
    {

        public ObservableCollection<Movie> Movies { get; set; }
        public RelayCommand AddMovieCommand { get; set; }

        public MoviesViewModel()
        {
            Movies = new ObservableCollection<Movie>(DummyDataSource.Movies);
            AddMovieCommand = new RelayCommand(() => AddMovie());
        }

        private void AddMovie()
        {
            Movies.Add(new Movie()
            {
                Title = "Test Title",
                Description = "Dummy description",
                Director = "Dummy Director"
            });


        }
    }
}
