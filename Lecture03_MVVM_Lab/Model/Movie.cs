﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03_MVVM_Lab.Model
{
    public class Movie : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private String title;
        public String Title
        {
            get { return title; }

            set
            {
                this.title = value;
                RaisePropertyChanged();
            }
        }
        private String director;
        public String Director
        {
            get { return director; }
            set
            {
                this.director = value;
                RaisePropertyChanged();
            }
        }

        private String description;
        public String Description
        {
            get { return description; }
            set
            {
                this.description = value;
                RaisePropertyChanged();
            }
        }

    }
}
