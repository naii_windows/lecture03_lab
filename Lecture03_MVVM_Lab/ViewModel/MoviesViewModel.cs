﻿using Lecture03_MVVM_Lab.Model;
using Lecture03_MVVM_Lab.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture03_MVVM_Lab.ViewModel
{
    public class MoviesViewModel
    {

        public ObservableCollection<Movie> Movies { get; set; }
        public RelayCommand SaveMovieCommand { get; set; }
        public MoviesViewModel()
        {
            Movies = new ObservableCollection<Movie>(DummyDataSource.Movies);
            SaveMovieCommand = new RelayCommand((p) => SaveMovie(p));
        }

        private void SaveMovie(object p)
        {
            this.Movies.Add(new Movie() { Title = p.ToString(),
                Description = "Dummy description",
                Director = "Dummy Director" });
        }


    }
}
